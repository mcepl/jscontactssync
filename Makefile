FILES=listcontacts.js

%.js: %.es
	babel --source-maps true -t ES5 -m commonStrict -o $@ $<

all: index.html $(FILES)

hesla-chrome.zip:
	zip -9vT $@ google9815148f4ac1407f.html hesla.appcache \
	    hesla.js icon-128.png icon-30.png icon-60.png \
	    index_de.html index.html manifest.json screen.css

install: $(FILES)
        : rsync -avLz --delete $(FILES) /home/matej/Dokumenty/website/luther/hesla/
	websync

clean:
	rm -fv *~ listcontacts.js *.js.map
