var dav = require('dav');

var xhr = new dav.transport.Basic(
    new dav.Credentials({
      username: 'matej',
    })
);

dav.createAccount({
    server: 'http://localhost:5232/matej/contacts.vcf',
    // server: 'https://hubmaier.ceplovi.cz/remote.php/carddav/addressbooks/matej/contacts',
    xhr: xhr,
    accountType: 'carddav'
})
.then(function(account) {
  account.addressBooks.forEach(function(addressBook){
    addressBook.objects.forEach(function(object){
      console.log('\nFound object data\n' + object.addressData);
    })
  })
});
